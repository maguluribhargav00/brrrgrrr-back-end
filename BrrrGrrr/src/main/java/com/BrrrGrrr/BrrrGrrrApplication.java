package com.BrrrGrrr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrrrGrrrApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrrrGrrrApplication.class, args);
	}

}
